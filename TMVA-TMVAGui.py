#!/usr/bin/env python
# coding: utf-8

# In[1]:


import ROOT
from ROOT import TMVA

inputFile   = "TMVA-Classification-Higgs-MVA-Comparison.root"
datasetName = "dataset"

mvas = TMVA.mvas(datasetName, inputFile)


# In[2]:


get_ipython().run_line_magic('jsroot', 'on')


# In[3]:


print ROOT.gROOT.GetListOfCanvases()


# In[ ]:




