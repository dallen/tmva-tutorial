#!/usr/bin/env python
# coding: utf-8

# # Comparing multiple BDT algorithms with TMVA
# 
# In this tutorial we will compare several algorithms with the [Toolkit for Multivariate Data Analysis with ROOT (TMVA)](http://root.cern/tmva) to classify events from particle collisions to determine whether they contain Higgs bosons.
# 
# We will skip the analysis of the input dataset (covered in another tutorial) and proceed to the comparison of several Boosted Decision Trees (BDT).

# In[1]:


# Importing the ROOT module
import ROOT
# Importing the TMVA, TFile (to open ROOT files) and TCut (to filter data)
from ROOT import TMVA, TFile, TCut

# Initialising TMVA
TMVA.Tools.Instance()
TMVA.PyMethodBase.PyInitialize();


# In[2]:


# This will overwrite the file at each execution
outputFile = TFile( "TMVA-Classification-Higgs-BDT-Comparison.root", "RECREATE" ) 
factory = TMVA.Factory( "TMVAClassification", outputFile,
                        "!V:Color:DrawProgressBar:Transformations=I;D;P;G,D:AnalysisType=Classification" )

# Opening the input data
inputFileName = "Higgs_data.root"
inputFile = TFile.Open( inputFileName )

# Retrieve input trees
sigTree = inputFile.Get("sig_tree")
bkgTree = inputFile.Get("bkg_tree")

#Instantiating the DataLoader
loader = TMVA.DataLoader("dataset")

# Global event weights per tree: useful to adjust the event weight to match a given luminosity
sigWeight = 1.0
bkgWeight = 1.0

# Note: one can add any number of trees (e.g. multiple backgrounds)
loader.AddSignalTree    ( sigTree, sigWeight )
loader.AddBackgroundTree( bkgTree, bkgWeight )

# Adding variables (here corresponding to the branches of the TTree)
loader.AddVariable("m_jj")
loader.AddVariable("m_jjj")
loader.AddVariable("m_lv")
loader.AddVariable("m_jlv")
loader.AddVariable("m_bb")
loader.AddVariable("m_wbb")
loader.AddVariable("m_wwbb")

# Apply additional cuts on the signal and background samples (can be different)
sigCut = TCut("")   ## for example: TCut sigCut = "m_jj>100"
bkgCut = TCut("")   ## for example: TCut bkgCut = "m_jj>100"


loader.PrepareTrainingAndTestTree( sigCut, bkgCut,
         "nTrain_Signal=7000:nTrain_Background=7000:SplitMode=Random:"
         "NormMode=NumEvents:!V" )


# Booking the BDT
factory.BookMethod(loader,ROOT.TMVA.Types.kBDT, "BDT",
          "!V:NTrees=200:MinNodeSize=2.5%:MaxDepth=2:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:"
          "BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20" )


# Booking another BDT. Note that the methog has a different name
factory.BookMethod(loader,ROOT.TMVA.Types.kBDT, "BDT2",
          "!V:NTrees=100:MinNodeSize=2.5%:MaxDepth=2:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:"
          "BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20" )

# Yet another BDT
factory.BookMethod(loader,ROOT.TMVA.Types.kBDT, "BDT3",
          "!V:NTrees=400:MinNodeSize=2.5%:MaxDepth=2:BoostType=AdaBoost:AdaBoostBeta=0.5:UseBaggedBoost:"
          "BaggedSampleFraction=0.5:SeparationType=GiniIndex:nCuts=20" )

factory.TrainAllMethods()
factory.TestAllMethods()
factory.EvaluateAllMethods()


# In[3]:


# We can now compare with our first BDT
cROC = factory.GetROCCurve(loader)
cROC.Draw()


# In[4]:


# Closing the file where we have the methods trained
outputFile.Close()
#factory.Delete()


# ## Follow-up
# 
# You can test various BDT configurations to understand how these algorithms work.
# 
# In another tutorial, we compare with other BDT and NN algorithms.
# 
# ### Exploring the methods using the TMVAGui
# 
# TMVA comes with the TMVAGui, which helps understand how the algorithms behave. To run it, simply run
# ```
# root -l -e 'TMVA::TMVAGui("TMVA-Classification-Higgs-BDT-Comparison.root")'
# ```
# from your terminal.
