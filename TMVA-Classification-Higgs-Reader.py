#!/usr/bin/env python
# coding: utf-8

# # TMVA Reader
# 
# In this tutorial we use the TMVA::Reader to apply our MVA discriminants to our dataset, or any new dataset (with the same input variables). This is the kind of code one uses to deploy the algorithm.
# 
# We start by intializing the session as necessary.

# In[1]:


# Importing the ROOT module
import ROOT
# Importing TMVA
from ROOT import TMVA

# Specifying the Keras backend
import os
os.environ["KERAS_BACKEND"] = "tensorflow"

# Initialising TMVA
TMVA.Tools.Instance()
TMVA.PyMethodBase.PyInitialize()


# We open the input file and confirm that the signal and background trees are present (using `TFile::ls()`).

# In[2]:


inputFile = ROOT.TFile("Higgs_data.root")
inputFile.ls()


# We now define local arrays to host the intput values and map them to the branches of the trees. Note that at this stage we don't need yet to provide the trees: this will be done later.

# In[3]:


reader = TMVA.Reader( "!Color:!Silent" )

# Defining the arrays that will store the input data
# Note: using C++ class member variable naming convention
from array import array
m_jj = array('f',[0])
m_jjj = array('f',[0])
m_lv = array('f',[0])
m_jlv = array('f',[0])
m_bb = array('f',[0])
m_wbb = array('f',[0])
m_wwbb = array('f',[0])

reader.AddVariable("m_jj",m_jj)
reader.AddVariable("m_jjj",m_jjj)
reader.AddVariable("m_lv",m_lv)
reader.AddVariable("m_jlv",m_jlv)
reader.AddVariable("m_bb",m_bb)
reader.AddVariable("m_wbb",m_wbb)
reader.AddVariable("m_wwbb",m_wwbb)


# We now load the various models we trained into the reader. The model parameters are stored at the following location: `dataset/weights/FactoryName_Algorithm.weights.xml`, where `dataset` is the name we previously gave to the loader.

# In[4]:


# Using the name of the factory created when training the algorithms
factoryName = "TMVAClassHiggsMVAComp"
methodNames = [ "BDT", "PyGTB", "PyRandomForest", "PyAdaBoost", "MLP", "DL_CPU", "Keras_Dense"]
for methodName in methodNames:
    weightsFile = "dataset/weights/" + factoryName + "_" + methodName + ".weights.xml"
    name = ROOT.TString( methodName )
    reader.BookMVA( name, weightsFile )


# Next we need to crate the histograms (or any other container) to hold the output of the classifiers for each event from the signal and background samples. We †create two dictionaties: one for signal and one for background. Each dictionary will have a key corresponding to each algorithm stored in `methodNames`. The output of our classifiers will be between -1 and 1 (some even between 0 and 1).

# In[5]:


sigHist = {}
bkgHist = {}
for name in methodNames:
    hName = "hSig_" + name
    hTitle = "Classifier Output for " + name 
    sigHist[name] = ( ROOT.TH1F(hName,hTitle+" (Sig)",100,-1,1) )
    hName = "hBkg_" + name
    bkgHist[name] = (ROOT.TH1F(hName,hTitle+" (Bkg)",100,-1,1) )


# We are now ready to process the input data and run it trough the reader. We start by allocating an array consisting in a vector of size equal to the number of events for each classifier. Then, we loop over the events in the TTree, saving the values of the branches in the predefined variables and computing the classifier output with `TMVA::Reader::EvaluateMVA()`.

# In[6]:


import numpy as np

# Getting the number of events in the TTree (to set the size of the output array)
nEvnt = inputFile.bkg_tree.GetEntries()

# The output array: one vector of size nEvnt for each classifier
vOut = np.arange( nEvnt * len(methodNames), dtype='float').reshape( len(methodNames), nEvnt)

# We loop over each event in the TTree
for iEvnt,entry in enumerate(inputFile.bkg_tree):
    m_jj[0] = entry.m_jj
    m_jjj[0] = entry.m_jjj
    m_lv[0] = entry.m_lv
    m_jlv[0] = entry.m_jlv
    m_bb[0] = entry.m_bb
    m_wbb[0] = entry.m_wbb
    m_wwbb[0] = entry.m_wwbb
    
    # We loop over each event 
    for i,methodName in enumerate(methodNames): 
        vOut[i,iEvnt] = reader.EvaluateMVA(methodName)
        bkgHist[methodName].Fill(vOut[i,iEvnt])        
        if (iEvnt%1000)==0 : print 'Event ',iEvnt,' MVA output for ',methodName,' =',vOut[i,iEvnt]
    
    # Uncomment the line below in case you dont' have time to run over the whole sample   
    # if (iEvnt > 4000) : break
vOutBkg = vOut


# 

# In[7]:


# Getting the number of events in the TTree (to set the size of the output array)
nEvnt = inputFile.sig_tree.GetEntries()

# The output array: one vector of size nEvnt for each classifier
vOut = np.arange( nEvnt * len(methodNames), dtype='float').reshape( len(methodNames), nEvnt)

# We loop over each event in the TTree
for iEvnt,entry in enumerate(inputFile.sig_tree):
    m_jj[0] = entry.m_jj
    m_jjj[0] = entry.m_jjj
    m_lv[0] = entry.m_lv
    m_jlv[0] = entry.m_jlv
    m_bb[0] = entry.m_bb
    m_wbb[0] = entry.m_wbb
    m_wwbb[0] = entry.m_wwbb
    
    # We loop over each event 
    for i,methodName in enumerate(methodNames): 
        vOut[i,iEvnt] = reader.EvaluateMVA(methodName)
        sigHist[methodName].Fill(vOut[i,iEvnt])        
        if (iEvnt%1000)==0 : print 'Event ',iEvnt,' MVA output for ',methodName,' =',vOut[i,iEvnt]
    
    # Uncomment the line below in case you dont' have time to run over the whole sample   
    # if (iEvnt > 4000) : break
vOutSig = vOut


# We now proceed with the plotting of the discriminant outputs, to check how our algorithms perform.

# In[8]:


cOut = ROOT.TCanvas("Discriminant Output", "", 1600, 400)
cOut.DivideSquare( len(methodNames) )
ROOT.gStyle.SetOptStat(0)
for i,name in enumerate(methodNames):
    pad = cOut.cd(i+1)
    bkgHist[name].Draw()
    sigHist[name].SetLineColor(ROOT.kRed)
    sigHist[name].Draw("same")
    pad.BuildLegend()
    
cOut.Draw()
cOut.Print(".pdf")


# In[ ]:




